LSL_BUILD_DIR=labstreaminglayer/build/install
LSL_INSTALL_DIR=/usr/local

all clean: $(SUBDIRS)

SUBDIRS=udptest eyeseecam-lsl

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)


libs: labstreaminglayer

labstreaminglayer:
	cd labstreaminglayer && \
	test -d build || (mkdir build && cd build && cmake ..) && \
	cd build && \
	$(MAKE)


.PHONY: labstreaminglayer
