#include "eyeseecam.h"
#include <iostream>

int
main (int argc, char *arg[])
{
  eyeseecam_udp_init ();
  eyeseecam_lsl_init ();

  while (true)
    {
      const double *lsldata = eyeseecam_udp_read ();
      if (!lsldata)
         std::cerr << "short read" << std::endl;
      else
         eyeseecam_lsl_write (lsldata);
    }
  eyeseecam_lsl_done ();
  eyeseecam_udp_done ();
}
