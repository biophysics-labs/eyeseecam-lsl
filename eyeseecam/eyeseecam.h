#include <unistd.h>

void eyeseecam_udp_init (int port = 12346);
double *eyeseecam_udp_read ();
void eyeseecam_udp_done ();
void eyeseecam_lsl_init ();
void eyeseecam_lsl_write (const double *buffer);
void eyeseecam_lsl_done ();
