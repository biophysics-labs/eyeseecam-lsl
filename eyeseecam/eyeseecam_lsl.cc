#include <lsl_cpp.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
using namespace lsl;
using namespace std;

static stream_outlet *outlet_m = 0;

void
eyeseecam_lsl_init ()
{
  const char *manufacturer = "DCN Biophysics GW";
  char application_version[255];
  sprintf (application_version, "EyeSeeTec LSL (build %s %s)", __DATE__,
           __TIME__);
  const char *model = "EyeSeeTec SCI";

  const char *labels_m[] = { "state",
                             "LeftTime",
                             "LeftSystemTime",
                             "RightTime",
                             "RightSystemTime",
                             "LeftFrameIndex",
                             "RightFrameIndex",
                             "HeadInertialAccelX",
                             "HeadInertialAccelXCal",
                             "HeadInertialAccelY",
                             "HeadInertialAccelYCal",
                             "HeadInertialAccelZ",
                             "HeadInertialAccelZCal",
                             "HeadInertialMagX",
                             "HeadInertialMagXCal",
                             "HeadInertialMagY",
                             "HeadInertialMagYCal",
                             "HeadInertialMagZ",
                             "HeadInertialMagZCal",
                             "HeadInertialPosX",
                             "HeadInertialPosXCal",
                             "HeadInertialPosY",
                             "HeadInertialPosYCal",
                             "HeadInertialPosZ",
                             "HeadInertialPosZCal",
                             "HeadInertialVelX",
                             "HeadInertialVelXCal",
                             "HeadInertialVelY",
                             "HeadInertialVelYCal",
                             "HeadInertialVelZ",
                             "HeadInertialVelZCal",
                             "LeftEyePosX",
                             "LeftEyePosY",
                             "LeftEyePosZ",
                             "LeftEyeVelX",
                             "LeftEyeVelY",
                             "LeftEyeVelZ",
                             "LeftPupilCol",
                             "LeftPupilCovXX",
                             "LeftPupilCovXY",
                             "LeftPupilCovYY",
                             "LeftPupilMethod",
                             "LeftPupilRow",
                             "LeftReflexCenterX",
                             "LeftReflexCenterY",
                             "RightEyePosX",
                             "RightEyePosY",
                             "RightEyePosZ",
                             "RightEyeVelX",
                             "RightEyeVelY",
                             "RightEyeVelZ",
                             "RightPupilCol",
                             "RightPupilCovXX",
                             "RightPupilCovXY",
                             "RightPupilCovYY",
                             "RightPupilMethod",
                             "RightPupilRow",
                             "RightReflexCenterX",
                             "RightReflexCenterY",
                             "TargetHor",
                             "TargetVer",
                             "visual_pos3",
                             "visual_pos4" };

  int num_channels_m = end (labels_m) - begin (labels_m);
  // for (const char* iter : labels_m)
  // num_channels_m++;
  double framerate = lsl::IRREGULAR_RATE;

  char hostname[255];
  gethostname (hostname, 255);
  char info_type[255];
  sprintf (info_type, "EyeSeeCam @ %s", hostname);

  char info_name[255];
  sprintf (info_name, "EyeSeeCam SCI Data");

  printf ("name=\"%s\"\ttype=\"%s\" nchan=\"%d\"\n\n", info_name, info_type, num_channels_m);
  stream_info info_m (info_name, info_type, num_channels_m, framerate,
                      lsl::channel_format_t (cft_double64),"EyeSeeCam");

  info_m.desc ()
      .append_child_value ("Manufacturer", manufacturer)
      .append_child_value ("Version", application_version)
      .append_child_value ("Model", model)
      .append_child_value ("Hostname", hostname);

  lsl::xml_element chns_m = info_m.desc ().append_child ("channels");
  for (const char *label : labels_m)
    {
      chns_m.append_child ("channel").append_child_value ("label", label);
    }
  outlet_m = new stream_outlet (info_m);

  /*
          // Convert LeftSystemTime to local time and split off nanoseconds
          time_t rawtime = LeftSystemTime / 1000000000;
          uint64_t ns = LeftSystemTime % 1000000000;
          struct tm ts;
          char LeftSystemTimeLocal[80];
          ts = *localtime(&rawtime);
          strftime(LeftSystemTimeLocal, sizeof(LeftSystemTimeLocal), "%a
     %Y-%m-%d %Z %H:%M:%S", &ts);

  */
}

void
eyeseecam_lsl_write (const double *lsldata)
{
  // IMPORTANT: lsldata, num_channels_m and the xml information must be matching
  // !!!!!
  if (outlet_m)
    outlet_m->push_sample (lsldata);
}

void
eyeseecam_lsl_done ()
{
  delete outlet_m;
}
