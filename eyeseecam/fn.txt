{
   "state",
   "LeftTime",
   "LeftSystemTime",
   "RightTime",
   "RightSystemTime",
   "LeftFrameIndex",
   "RightFrameIndex",
   "HeadInertialAccelX",
   "HeadInertialAccelXCal",
   "HeadInertialAccelY",
   "HeadInertialAccelYCal",
   "HeadInertialAccelZ",
   "HeadInertialAccelZCal",
   "HeadInertialMagX",
   "HeadInertialMagXCal",
   "HeadInertialMagY",
   "HeadInertialMagYCal",
   "HeadInertialMagZ",
   "HeadInertialMagZCal",
   "HeadInertialPosX",
   "HeadInertialPosXCal",
   "HeadInertialPosY",
   "HeadInertialPosYCal",
   "HeadInertialPosZ",
   "HeadInertialPosZCal",
   "HeadInertialVelX",
   "HeadInertialVelXCal",
   "HeadInertialVelY",
   "HeadInertialVelYCal",
   "HeadInertialVelZ",
   "HeadInertialVelZCal",
   "LeftEyePosX",
   "LeftEyePosY",
   "LeftEyePosZ",
   "LeftEyeVelX",
   "LeftEyeVelY",
   "LeftEyeVelZ",
   "LeftPupilCol",
   "LeftPupilCovXX",
   "LeftPupilCovXY",
   "LeftPupilCovYY",
   "LeftPupilMethod",
   "LeftPupilRow",
   "LeftReflexCenterX",
   "LeftReflexCenterY",
   "RightEyePosX",
   "RightEyePosY",
   "RightEyePosZ",
   "RightEyeVelX",
   "RightEyeVelY",
   "RightEyeVelZ",
   "RightPupilCol",
   "RightPupilCovXX",
   "RightPupilCovXY",
   "RightPupilCovYY",
   "RightPupilMethod",
   "RightPupilRow",
   "RightReflexCenterX",
   "RightReflexCenterY",
   "TargetHor",
   "TargetVer",
   "visual_pos3",
   "visual_pos4",
   0
}
