//
//  udptest.c
//  Example C code to receive UDP packets from EyeSeeCam Sci
//  Version: 1.0
//
//  Created by Stefan Kohlbecher on 27.10.22.
//

#include <arpa/inet.h>
#include <stdio.h>
#include <time.h>

#define PORT 12346

int main(int argc, char *arg[]) {
    int in_sock_fd;
    struct sockaddr_in sin;
    //GW uint32_t buffer[276];
    uint8_t buffer[276];
    in_sock_fd = socket(PF_INET, SOCK_DGRAM, 0);
    
    sin.sin_family = AF_INET;
    sin.sin_port = htons(PORT);
    sin.sin_addr.s_addr = INADDR_ANY;
    bind(in_sock_fd, (struct sockaddr*)&sin, sizeof(sin));
    while (1) {
        ssize_t bytes_received = recvfrom(in_sock_fd,
                                          &buffer,
                                          sizeof(buffer),
                                          0,
                                          NULL,
                                          NULL);
        
        uint32_t header = buffer[0];
        uint64_t *group2 = (uint64_t *)&buffer[1];
        uint32_t *group3 = &buffer[13];
        
        // Convert all values from network byte order to host byte order
        header = ntohl(header);
        for (int i=0; i<6; i++)
            group2[i] = ntohll(group2[i]);
        for (int i=0; i<56; i++)
            group3[i] = ntohl(group3[i]);
        float *group3_as_float = (float *)group3;
        
        uint8_t state = (header&0xff) >> 1; // 0: Idle, 1: Prepared, 2: Recording, 7: Unknown
        
        uint64_t LeftTime        = group2[0];
        uint64_t LeftSystemTime  = group2[1];
        uint64_t RightTime       = group2[2];
        uint64_t RightSystemTime = group2[3];
        uint64_t LeftFrameIndex  = group2[4];
        uint64_t RightFrameIndex = group2[5];
        
        float HeadInertialAccelX    = group3_as_float[0];
        float HeadInertialAccelXCal = group3_as_float[1];
        float HeadInertialAccelY    = group3_as_float[2];
        float HeadInertialAccelYCal = group3_as_float[3];
        float HeadInertialAccelZ    = group3_as_float[4];
        float HeadInertialAccelZCal = group3_as_float[5];
        float HeadInertialMagX      = group3_as_float[6];
        float HeadInertialMagXCal   = group3_as_float[7];
        float HeadInertialMagY      = group3_as_float[8];
        float HeadInertialMagYCal   = group3_as_float[9];
        float HeadInertialMagZ      = group3_as_float[10];
        float HeadInertialMagZCal   = group3_as_float[11];
        float HeadInertialPosX      = group3_as_float[12];
        float HeadInertialPosXCal   = group3_as_float[13];
        float HeadInertialPosY      = group3_as_float[14];
        float HeadInertialPosYCal   = group3_as_float[15];
        float HeadInertialPosZ      = group3_as_float[16];
        float HeadInertialPosZCal   = group3_as_float[17];
        float HeadInertialVelX      = group3_as_float[18];
        float HeadInertialVelXCal   = group3_as_float[19];
        float HeadInertialVelY      = group3_as_float[20];
        float HeadInertialVelYCal   = group3_as_float[21];
        float HeadInertialVelZ      = group3_as_float[22];
        float HeadInertialVelZCal   = group3_as_float[23];
        float LeftEyePosX           = group3_as_float[24];
        float LeftEyePosY           = group3_as_float[25];
        float LeftEyePosZ           = group3_as_float[26];
        float LeftEyeVelX           = group3_as_float[27];
        float LeftEyeVelY           = group3_as_float[28];
        float LeftEyeVelZ           = group3_as_float[29];
        float LeftPupilCol          = group3_as_float[30];
        float LeftPupilCovXX        = group3_as_float[31];
        float LeftPupilCovXY        = group3_as_float[32];
        float LeftPupilCovYY        = group3_as_float[33];
        float LeftPupilMethod       = group3_as_float[34];
        float LeftPupilRow          = group3_as_float[35];
        float LeftReflexCenterX     = group3_as_float[36];
        float LeftReflexCenterY     = group3_as_float[37];
        float RightEyePosX          = group3_as_float[38];
        float RightEyePosY          = group3_as_float[39];
        float RightEyePosZ          = group3_as_float[40];
        float RightEyeVelX          = group3_as_float[41];
        float RightEyeVelY          = group3_as_float[42];
        float RightEyeVelZ          = group3_as_float[43];
        float RightPupilCol         = group3_as_float[44];
        float RightPupilCovXX       = group3_as_float[45];
        float RightPupilCovXY       = group3_as_float[46];
        float RightPupilCovYY       = group3_as_float[47];
        float RightPupilMethod      = group3_as_float[48];
        float RightPupilRow         = group3_as_float[49];
        float RightReflexCenterX    = group3_as_float[50];
        float RightReflexCenterY    = group3_as_float[51];
        float TargetHor             = group3_as_float[52];
        float TargetVer             = group3_as_float[53];
        float visual_pos3           = group3_as_float[54];
        float visual_pos4           = group3_as_float[55];
        
        // Convert LeftSystemTime to local time and split off nanoseconds
        time_t rawtime = LeftSystemTime / 1000000000;
        uint64_t ns = LeftSystemTime % 1000000000;
        struct tm ts;
        char LeftSystemTimeLocal[80];
        ts = *localtime(&rawtime);
        strftime(LeftSystemTimeLocal, sizeof(LeftSystemTimeLocal), "%a %Y-%m-%d %Z %H:%M:%S", &ts);
        
        printf("%s.%03llu: state=%d, LeftTime=%llu, LeftSystemTime=%llu, LeftFrameIndex=%llu, LeftPupilCol=%f, LeftPupilRow=%f\n",
               LeftSystemTimeLocal,
               ns/1000000,
               state,
               LeftTime,
               LeftSystemTime,
               LeftFrameIndex,
               LeftPupilCol,
               LeftPupilRow);
    }
    return 0;
}
